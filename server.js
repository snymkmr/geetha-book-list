var express = require('express');
var app = express();
var mongojs = require("mongojs");
var bodyParser = require("body-parser");
var db = mongojs('bookList', ['bookList']);

app.use(express.static(__dirname + "/public"));
app.use(bodyParser.json());

app.get('/book-list', function (req, res) {
	db.bookList.find(function (err, docs) {
		res.json(docs);
	});
});

app.post('/book-list', function (req, res) {
	db.bookList.insert(req.body, function (err, doc) {
		res.json(doc);
	});
});

app.get('/book-list/:id', function (req, res) {
	var id = req.params.id;
	db.bookList.findOne({ _id: mongojs.ObjectId(id) }, function (err, doc) {
		res.json(doc);
	});
});

app.put('/book-list/:id', function (req, res) {
	var id = req.params.id;
	db.bookList.findAndModify({
		query: { _id: mongojs.ObjectId(id) },
		update: { $set: { bookName: req.body.bookName, bookId: req.body.bookId } },
		new: true
	}, function (err, doc) {
		res.json(doc);
	});
});

app.delete('/book-list/:id', function (req, res) {
	var id = req.params.id;
	db.bookList.remove({ _id: mongojs.ObjectId(id) }, function (err, doc) {
		res.json(doc);
	});
});

app.listen(3000);
console.log('server is running!');