var app = angular.module('myApp', []);
app.controller('myAppCtrl', function ($scope, $http) {

	$scope.hideAddBtn = false;

	var getBookList = function () {
		$http.get('/book-list').success(function (res) {
			$scope.bookList = res;
			$scope.bookObj = '';
		});
	};

	$scope.addBook = function (obj) {			
		if (obj.bookName && obj.bookId) {
			$http.post('/book-list', obj).success(function (res) {
				getBookList();
			});
		} else {
			$('#errorModal').modal('show');
		}
	};

	$scope.editBook = function (obj) {
		$scope.hideAddBtn = true;
		$http.get('/book-list/' + obj._id).success(function (res) {
			$scope.bookObj = res;
		});
	};

	$scope.deleteBook = function (obj) {
		$http.delete('/book-list/' + obj._id).success(function (res) {
			getBookList();
		});
	};

	$scope.save = function (obj) {		
		$scope.hideAddBtn = false;
		$http.put('/book-list/' + obj._id, obj).success(function (res) {
			getBookList();
		});
	}
	
	getBookList();
});



